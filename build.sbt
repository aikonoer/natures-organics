name := """natures-organics"""
organization := "com.natures-organics"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
val playReactiveMongo = "0.12.7-play26"
val silhouetteVersion = "5.0.2"
val playS3Version = "9.0.0"
val scrimageVersion = "3.0.0-alpha4"

scalaVersion := "2.12.3"
resolvers += "Atlassian Releases" at "https://maven.atlassian.com/public/"
herokuAppName in Compile := "natures-organics"

libraryDependencies ++= Seq(
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.reactivemongo" %% "play2-reactivemongo" % playReactiveMongo,
  "com.mohiva" %% "play-silhouette" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-password-bcrypt" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-crypto-jca" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-persistence" % silhouetteVersion,
  "com.mohiva" %% "play-silhouette-testkit" % silhouetteVersion % "test",
  "net.kaliber" %% "play-s3" % playS3Version,
  "com.sksamuel.scrimage" %% "scrimage-core" % scrimageVersion
)

