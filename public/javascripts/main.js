$(document).ready(function () {
    $('#login-anchor-modal').click(function (e) {
        $('#register-modal').modal("hide");
        $('#login-modal').modal("show");
    });

    $('#register-anchor-modal').click(function (e) {
        $('#login-modal').modal("hide");
        $('#register-modal').modal("show");
    });

    $('#login-close-alert').click(function (e) {
        $('#login-alert').hide();
    });



    $("form#login").submit(function (e) {
        e.preventDefault();
        console.log("logging in...");
        var username = $('#login-username').val();
        var password = $('#login-password').val();
        var data = {
            username : username,
            password : password
        };
        console.log($(this).attr("action"));
        $.post({
            url: $(this).attr("action"),
            data: JSON.stringify(data),
            contentType : "application/json",
            success: function (data) {
                console.log('Login successful');
                window.location.reload();
                $("#login-modal").modal("hide");
            },
            error: function (error) {
                $('#login-alert').show();
                console.log('An error occurred.');
                console.log(error);
            }
        });
    });

    $("form#register").submit(function (e) {
        e.preventDefault();
        console.log("Registering...");
        var username = $('#register-username').val();
        var firstName = $('#register-firstName').val();
        var lastName = $('#register-lastName').val();
        var email = $('#register-email').val();
        var phone = $('#register-phone').val();
        var password = $('#register-password').val();
        var data = {
            username : username,
            firstName : firstName,
            lastName : lastName,
            email : email,
            phoneNumber : phone,
            password : password
        };
        console.log($(this).attr("action"));
        $.post({
            url: $(this).attr("action"),
            data: JSON.stringify(data),
            contentType : "application/json",
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
                window.location.href = "/";
                $("#login-modal").modal("hide");
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
                $("#login-modal").modal("hide");
            }
        });
    });
    
    $('#signoutBtn').click(function (e) {
        var u = authJSRoutes.controllers.AuthController.logout();
        $.get({
            url: u.url,
            success: function (s) {
                $('#signout-modal').modal("hide");
                window.location.href = "/"
            },
            error: function (e) {
                console.log("Error occurred")
            }
        })
    });
});

function imgError(image) {
    console.log("onerror");
    image.onerror = "";
    image.src = "https://s3-ap-southeast-2.amazonaws.com/marcusbucketsresizedimages/images/default.jpg";
    return true;
    }