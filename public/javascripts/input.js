$(document).ready(function(){
    tinymce.init({
        selector: '#textArea',
        height: 500,
        menubar: false,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help',
            'save fullscreen preview'
        ],
        toolbar: 'save | preview | fullscreen | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'],
        save_onsavecallback: function () { saveContents(); },
        setup: function(ed) {
            ed.on('keyup', function(e) {
                preview(ed.getContent())
            });
        }
    });

    $("#preview").click(function () {
        var content = tinymce.activeEditor.getContent();
        $("#data-container").toggle(function () {
            preview(content)
        });

    });

    function saveContents(){
        var content = tinymce.activeEditor.getContent();
        var text = tinymce.activeEditor.getContent({format: "text"}).replace(/\n/g, " ");
        var title = $('#title').val();
        var url = title.split(" ").join("-").toLowerCase();
        var note =
            {username : "default",
                title : title,
                html : content,
                text: text,
                url: url
            };
        var u = homeJSRoutes.controllers.HomeController.insertNote();
        $('#data-container').html(content);
        $.post({
            url: u.url,
            data: JSON.stringify(note),
            success: function(data, status) {
                console.log(data)
                console.log(status)
            },
            contentType : "application/json",
            dataType: "json"
        });
        return false;
    }

    function preview(content) {
        $('#data-container').html(content)
    }
});