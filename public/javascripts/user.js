$(document).ready(function () {
    var role = $('#roles-menu').text().trim();
    console.log(role);

    var roles = $('#roles');

    roles
        .find('li')
        .filter(function () {
            console.log($(this).text());
            return $(this).text() === role;
        })
        .hide();

    roles.on("click", ".dropdown-list", function (e) {
        var role = $(this).text();
        $('#roles-menu')
            .html(role)
            .append(" <span class='caret'> </span>");
        $('#update-role')
            .removeClass("disabled")
            .addClass("active");
        console.log(role);
    });

    $('#update-role').click(function (e) {
        var role = $('#roles-menu').text().toLowerCase().trim();
        var user = $('#user').text().toLowerCase().trim();
        console.log(role);
        console.log(user);
        var u = authJSRoutes.controllers.AuthController.updateRole();
        var userDetails =
            {
                "username": user,
                "role": role
            };
        $.get({
            url: u.url,
            data: userDetails,
            success: function (data, status) {
                console.log(data);
                console.log(status);
                $('#update-role')
                    .removeClass("active")
                    .addClass("disabled");
                roles
                    .find('li')
                    .show();

                roles
                    .find('li')
                    .filter(function () {
                        return $(this).text().toLowerCase() === role;
                    })
                    .hide();
            }
        });
    });

});
