$(document).ready(function () {
    $('#search-type').keypress(function (e) {
        var key = e.which;
        if (key === 13) {
            search()
        }
    });

    $('#search-btn').click(function () {
        search()
    });
});

function search() {
    $('#results').empty();
    var query = $('#search-type').val();
    $.getJSON(homeJSRoutes.controllers.HomeController.findNote(), {"query": query},
        function (data) {
            $.each(data, function (key, value) {
                var url = value.title.replace(/\s+/g, '-').toLowerCase();
                var link = homeJSRoutes.controllers.HomeController.getNote(url);
                $('#results').append('<div class="list-group-item">'
                    + '<h2><a href=' + link.url + '>' + value.title + '</a></h2>'
                    + '<small>' + 'by: ' + value.username + '</small>'
                    + '</div>')
            })
        })
}