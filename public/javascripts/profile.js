$(document).ready(function(){
    $("#change-picture").click(function(){
        console.log("clicked picture");
        $("#picture").click();
    });

    $("#picture").change(function(){
        console.log("picture submit");
        var formData = new FormData($("form#data")[0]);

        $.post({
            url: $("form#data").attr("action"),
            data: formData,
            success: function (data) {
                window.location.reload();
                console.log("Successfully uploaded")
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });

    $('#update-details').click(function(){
      console.log("updating personal info");
      var firstName = getData("firstName");
      var lastName = getData("lastName");
      var phoneNumber = getData("phoneNumber");
      var email = getData("email");
      var data = {
          "firstName" : firstName,
          "lastName" : lastName,
          "phoneNumber" : phoneNumber,
          "email" : email
      };
      console.log(data);
      var u = authJSRoutes.controllers.AuthController.update();

      $.get({
          url: u.url,
          data: data,
          success: function (data) {
              console.log('Submission was successful.');
              console.log(data);
              window.location.reload();
          },
          error: function (data) {
              console.log('An error occurred.');
              console.log(data);
          }
      });
    });
});

function getData(key) {
    if($('#'+key).val().length == 0) {
        return $('#'+key).attr("placeholder");
    } else {
        return $('#'+key).val();
    }
};

function imgProfileError(image) {
        console.log("onerror");
        image.onerror = "";
        image.src = "https://s3-ap-southeast-2.amazonaws.com/marcusbucketsresizedimages/images/default.jpg";
        return true;
        }
