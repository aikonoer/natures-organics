package models.auth

import play.api.libs.json.{Json, OFormat}

case class LoginForm(username: String,
                     password: String)

object LoginForm {
  implicit val loginCredentialsFormat: OFormat[LoginForm] = Json.format[LoginForm]
}

case class RegisterForm(username: String,
                        firstName: String,
                        lastName: String,
                        email: String,
                        phoneNumber: String,
                        password: String
                        )

object RegisterForm {
  implicit val registerFormFormat: OFormat[RegisterForm] = Json.format[RegisterForm]
}

