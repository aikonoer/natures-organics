package models.auth
import play.api.data._
import play.api.data.Forms._
import com.mohiva.play.silhouette.api.Identity
import models.Model
import play.api.libs.json.{Json, OFormat}

case class User(username: String,
                firstName: String,
                lastName: String,
                email: String,
                phoneNumber: Int,
                role: String = User.GUEST) extends Identity with Model

object User {
  implicit val userFormat: OFormat[User] = Json.format[User]

  val GUEST = "guest"
  val OPERATOR = "operator"
  val MAINTENANCE = "maintenance"
  val ADMIN = "admin"

  def checkRole(role: String): Boolean = {
    Seq(GUEST, OPERATOR, MAINTENANCE, ADMIN).contains(role)
  }

  val editForm = Form(
    tuple(
      "username" -> text,
      "firstName" -> text,
      "lastName" -> text,
      "phoneNumber" -> number,
      "email" -> email
    )
  )

}


case class PasswordDetails(hasherId: String,
                           username: String,
                           hashedPassword: String,
                           salt: Option[String]) extends Model

object PasswordDetails {
  implicit val passwordFormat: OFormat[PasswordDetails] = Json.format[PasswordDetails]
}