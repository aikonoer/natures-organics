package services

import javax.inject.Inject

import models.Note
import repositories.NoteRepo

import scala.concurrent.{ExecutionContext, Future}

trait NoteService {
  def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]]
  def find(implicit ec: ExecutionContext): Future[List[Note]]
  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]]
  def findText(words: String)(implicit ec: ExecutionContext): Future[List[Note]]
  def findByURL(url: String)(implicit ec: ExecutionContext): Future[Option[Note]]
}

class NoteServiceImpl @Inject()(noteRepo: NoteRepo) extends NoteService {
  def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]] = noteRepo.insert(t)
  def find(implicit ec: ExecutionContext): Future[List[Note]] = noteRepo.find
  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]] = noteRepo.findByUsername(username)
  def findText(words: String)(implicit ec: ExecutionContext): Future[List[Note]] = noteRepo.findText(words)
  def findByURL(url: String)(implicit ec: ExecutionContext): Future[Option[Note]] = {
    noteRepo.findByURL(url)
  }
}
