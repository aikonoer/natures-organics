package services.auth

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import models.auth.User
import repositories.auth.UserInfoRepo

import scala.concurrent.{ExecutionContext, Future}

trait UserInfoService extends IdentityService[User]{
  def insert(t: User)(implicit ec: ExecutionContext): Future[Either[String, User]]
  def updateRole(user: User): Future[Either[String, User]]
  def update(user: User): Future[Either[String, User]]
  def findAll(): Future[List[User]]
}

class UserInfoServiceImpl @Inject()(userInfoRepo: UserInfoRepo) extends UserInfoService {

  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = userInfoRepo.findByUsername(loginInfo)
  def insert(t: User)(implicit ec: ExecutionContext): Future[Either[String, User]] = userInfoRepo.insert(t)
  def updateRole(user: User): Future[Either[String, User]] = userInfoRepo.updateRole(user)
  def update(user: User): Future[Either[String, User]] = userInfoRepo.update(user)
  def findAll(): Future[List[User]] = userInfoRepo.findAll()
}
