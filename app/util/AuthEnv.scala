package util

import com.mohiva.play.silhouette.api.Env
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.auth.User

trait AuthEnv extends Env {
  override type I = User
  override type A = CookieAuthenticator
}
