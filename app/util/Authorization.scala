package util

import com.mohiva.play.silhouette.api.Authorization
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.auth.User
import play.api.mvc.Request

import scala.concurrent.Future

case class WithRole(roles: String*) extends Authorization[User, CookieAuthenticator] {

  def isAuthorized[B](user: User, authenticator: CookieAuthenticator)(implicit request: Request[B]): Future[Boolean] =
    Future.successful(roles.contains(user.role))
}