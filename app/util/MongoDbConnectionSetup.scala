package util

import javax.inject.Inject

import play.api.Configuration
import reactivemongo.api.{MongoConnection, MongoDriver}

class MongoDbConnectionSetup @Inject()(config: Configuration) {
  private val uri: Option[String] = config.getOptional[String]("mongo.uri")
  val driver = new MongoDriver

  val connectSetup: MongoConnection = uri.fold(driver.connection(List("localhost:9000"))) { u =>
    MongoConnection.parseURI(u).map { p =>
      val conOpts = p.options.copy(authenticationDatabase = Some("admin"), sslEnabled = true)
      val authOpts = p.authenticate.map(_.copy(db = "admin"))
      val uri = p.copy(options = conOpts, authenticate = authOpts)
      driver.connection(uri)
    }.get
  }
}
