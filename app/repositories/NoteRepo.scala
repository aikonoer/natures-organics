package repositories

import javax.inject.{Inject, Singleton}

import models.Note
import play.api.libs.json.{JsObject, Json}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{Cursor, MongoConnection, ReadPreference}
import reactivemongo.bson.BSONDocument
import reactivemongo.play.json._
import reactivemongo.play.json.commands.JSONAggregationFramework._

import scala.concurrent.{ExecutionContext, Future}

trait NoteRepo extends MongoDb {
  def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]]
  def find(implicit ec: ExecutionContext): Future[List[Note]]
  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[Traversable[Note]]
  def findText(words: String)(implicit ec: ExecutionContext): Future[List[Note]]
  def findByURL(url: String)(implicit ec: ExecutionContext): Future[Option[Note]]
}

@Singleton
class NoteRepoImpl @Inject()(val mongoConnection: MongoConnection)(implicit ec: ExecutionContext) extends NoteRepo {

  private val index = Index(
    Seq("title" -> IndexType.Text, "text" -> IndexType.Text),
    name = Some("titleIndex"),
    options =
      BSONDocument("weights" -> BSONDocument("title" -> 10, "text" -> 5))
  )

  noteColl.flatMap(_.indexesManager.ensure(index))

  private def noteColl = connect(coll = "note")

  override def insert(t: Note)(implicit ec: ExecutionContext): Future[Either[String, Note]] = {
    noteColl.flatMap(_.insert(t))
      .map(_ => Right(t))
      .recover {
        case WriteResult.Message(msg) => Left(msg)
      }
  }

  def find(implicit ec: ExecutionContext): Future[List[Note]] =
    genericFind(Json.obj(), Json.obj())

  def findByUsername(username: String)(implicit ec: ExecutionContext): Future[List[Note]] =
    genericFind(Json.obj("username" -> username), Json.obj())

  def findText(words: String)(implicit ec: ExecutionContext): Future[List[Note]] = {
    val firstOp = Match(Json.obj("$text" -> Json.obj("$search" -> words)))
    val pipeline = List(Sort(MetadataSort("score", TextScore)))
    val cursor: Future[Cursor[Note]] = noteColl.map(_.aggregatorContext[Note](firstOp, pipeline).prepared.cursor)
    cursor.flatMap(_.collect[List](Int.MaxValue, Cursor.FailOnError[List[Note]]()))
  }

  def findByURL(url: String)(implicit ec: ExecutionContext): Future[Option[Note]] =
    noteColl
      .flatMap(_.find(Json.obj("url" -> url))
        .one[Note](ReadPreference.Primary))

  private def genericFind(find: JsObject, projection: JsObject)(implicit ec: ExecutionContext) = {
    noteColl
      .map(_.find(find, projection)
        .cursor[Note](ReadPreference.Primary))
      .flatMap(_.collect[List](-1, Cursor.FailOnError[List[Note]]()))
  }
}
