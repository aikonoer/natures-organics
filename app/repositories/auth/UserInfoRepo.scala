package repositories.auth

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import models.auth.User
import play.api.Logger
import play.api.libs.json.Json
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.{Cursor, MongoConnection, ReadPreference}
import reactivemongo.play.json._
import repositories.MongoDb

import scala.concurrent.{ExecutionContext, Future}

trait UserInfoRepo extends MongoDb {
  def insert(user: User)(implicit ec: ExecutionContext): Future[Either[String, User]]
  def findByUsername(loginInfo: LoginInfo): Future[Option[User]]
  def updateRole(user: User): Future[Either[String, User]]
  def update(user: User): Future[Either[String, User]]
  def findAll(): Future[List[User]]
}

class UserInfoRepoImpl @Inject()(val mongoConnection: MongoConnection)(implicit ec: ExecutionContext) extends UserInfoRepo {

  private val index = Index(Seq((
    "username",
    IndexType.Ascending)),
    Some("unique-username"), unique = true)

  private def userInfoColl = connect(coll = "userInfo")

  userInfoColl.flatMap(_.indexesManager.ensure(index))

  def insert(user: User)(implicit ec: ExecutionContext): Future[Either[String, User]] = {
    Logger.info("user info repo INSERT")
    for {
      userColl <- userInfoColl
      w <- userColl.insert(user)
    } yield
      if (w.ok) Right(user) else Left("Insert not successful")
  }
  def findByUsername(loginInfo: LoginInfo): Future[Option[User]] = {
    Logger.info(s"user info repo findByUsername ${loginInfo.providerKey}")
    userInfoColl
      .flatMap(_.find(Json.obj("username" -> loginInfo.providerKey), Json.obj("_id" -> 0))
        .one[User])
  }

  def updateRole(user: User): Future[Either[String, User]] = {
    Logger.info(s"updating role to ${user.username}")
    userInfoColl
      .flatMap(_.update(
        Json.obj("username" -> user.username),
        Json.obj("$set" -> Json.obj("role" -> user.role))))
      .map(res => if(res.ok) Right(user) else Left("Update not successful"))
  }

  def update(user: User): Future[Either[String, User]] = {
    Logger.info(s"Updating ${user.username} info")
    userInfoColl
      .flatMap(_.update(
        Json.obj("username" -> user.username),
        Json.obj("$set" -> user)))
      .map(res => if(res.ok) Right(user) else Left("Update not successful"))
  }

  def findAll(): Future[List[User]] = {
    Logger.info(s"Find all users")
    userInfoColl
      .map(_.find(Json.obj(), Json.obj())
        .cursor[User](ReadPreference.Primary))
      .flatMap(_.collect[List](-1, Cursor.FailOnError[List[User]]()))
  }
}
