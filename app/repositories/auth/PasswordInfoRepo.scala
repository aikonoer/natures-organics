package repositories.auth

import javax.inject.{Inject, Singleton}

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import models.auth.PasswordDetails
import play.api.Logger
import play.api.libs.json.Json
import reactivemongo.play.json._
import reactivemongo.api.{MongoConnection, ReadPreference}
import repositories.MongoDb
import services.auth.UserInfoService

import scala.concurrent.{ExecutionContext, Future}

trait PasswordInfoRepo extends DelegableAuthInfoDAO[PasswordInfo] with MongoDb

@Singleton
class PasswordInfoRepoImpl @Inject()(val mongoConnection: MongoConnection,
                                     userInfoService: UserInfoService)(implicit ec: ExecutionContext)
  extends PasswordInfoRepo {

  private def passwordInfoColl = connect(coll = "passwordInfo")

  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] = {
    Logger.info("password info repo FIND")
    for {
      _ <- userInfoService.retrieve(loginInfo)
      c <- passwordInfoColl
      pw <- c.find(Json.obj("username" -> loginInfo.providerKey))
        .one[PasswordDetails](ReadPreference.Primary)
    } yield
      if (pw.isDefined)
        Some(PasswordInfo(pw.get.hasherId, pw.get.hashedPassword, pw.get.salt))
      else None
  }

  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    Logger.info("password info repo ADD")
    for {
      _ <- userInfoService.retrieve(loginInfo)
      c <- passwordInfoColl
      w <- c.insert(PasswordDetails(authInfo.hasher, loginInfo.providerKey, authInfo.password, authInfo.salt))
      if w.ok
    } yield authInfo
  }

  def update(loginInfo: LoginInfo, authInfo: PasswordInfo) = ???
  def save(loginInfo: LoginInfo, authInfo: PasswordInfo) = ???
  def remove(loginInfo: LoginInfo) = ???
}
