package controllers

import java.nio.file.Files
import javax.inject.Inject

import com.mohiva.play.silhouette.api.Silhouette
import fly.play.s3.BucketFile
import models.auth.User
import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}
import services.S3Service
import util.AuthEnv

import scala.concurrent.{ExecutionContext, Future}

class IOController @Inject()(cc: ControllerComponents,
                             s3Service: S3Service,
                             silhouette: Silhouette[AuthEnv]) extends AbstractController(cc) with MessageController {

  implicit lazy val ec: ExecutionContext = defaultExecutionContext
  implicit val messages: MessagesApi = messagesApi

  def postUpload: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>

    val formData = request.body.asMultipartFormData
    val picture = formData.get.file("picture")
    val content = views.html.profile(Some(request.identity))

    picture.map { pic =>
      Logger.info("image found")
      val bytesArray = Files.readAllBytes(pic.ref.path)
      val bucketFile = BucketFile(s"${request.identity.username}.jpg", pic.contentType.get, bytesArray)

      s3Service.insert(bucketFile).map {
        case Right(_) => Ok(content)
        case Left(_) => BadRequest(content)
      }

    }.getOrElse(Future.successful(Ok(content)))
  }
}
