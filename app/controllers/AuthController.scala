package controllers

import javax.inject.Inject

import com.mohiva.play.silhouette.api.util.{Credentials, PasswordHasher}
import com.mohiva.play.silhouette.api.{LoginInfo, Silhouette}
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import models.auth.{LoginForm, RegisterForm, User}
import play.api.i18n.{Messages, MessagesApi}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import play.api.routing.JavaScriptReverseRouter
import play.api.{Configuration, Logger}
import services.NoteService
import services.auth.{PasswordInfoService, UserInfoService}
import util.{AuthEnv, WithRole}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

class AuthController @Inject()(cc: ControllerComponents,
                               noteService: NoteService,
                               config: Configuration,
                               userInfoService: UserInfoService,
                               passwordInfoService: PasswordInfoService,
                               passwordHasher: PasswordHasher,
                               credentialsProvider: CredentialsProvider,
                               silhouette: Silhouette[AuthEnv]) extends AbstractController(cc) with MessageController {

  implicit lazy val ec: ExecutionContext = defaultExecutionContext
  implicit val messages: MessagesApi = messagesApi

  def postRegisterJson(): Action[JsValue] = silhouette.UnsecuredAction.async(parse.json) { implicit request =>
    Logger.info("User registration")
    request.body.validate[RegisterForm].fold(
      error => Future.successful(BadRequest(error.mkString(", "))),
      user => {
        Logger.info(s"username: ${user.username}, password: ${user.password}")
        val loginInfo = LoginInfo(CredentialsProvider.ID, user.username)
        for {
          ou <- userInfoService.retrieve(loginInfo)
          if ou.isEmpty
          e <- userInfoService.insert(User(user.username, user.firstName, user.lastName, user.email, user.phoneNumber.toInt))
          if e.isRight
          either <- passwordInfoService.add(loginInfo, passwordHasher.hash(user.password))
        } yield either match {
          case Right(_) => Ok(okResponse(Messages("add.success", user.username)))
          case Left(error) => BadRequest(error)
        }
      }
    )
  }

  def postLoginJson(): Action[JsValue] = silhouette.UnsecuredAction.async(parse.json) { implicit request =>
    request.body.validate[LoginForm].fold(
      error => Future.successful(BadRequest(error.mkString(", "))),
      user => {
        Logger.info(s"username: ${user.username}, password: ${user.password}")
        credentialsProvider
          .authenticate(Credentials(user.username, user.password))
          .flatMap(loginInfo =>
            userInfoService.retrieve(loginInfo)
              .flatMap {
                case Some(_) => for {
                  authenticator <- silhouette.env.authenticatorService.create(loginInfo)
                  cookie <- silhouette.env.authenticatorService.init(authenticator)
                  result <- silhouette.env.authenticatorService.embed(cookie,
                    Ok(okResponse(Messages("add.success", user.username))))
                } yield result
                case None => Future.successful(BadRequest(Json.obj("message" -> "Could not find user")))
              }
          )
      }
    )
  }

  def logout: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    Logger.info(s"${request.identity.username} is now logging out....")
    silhouette.env.authenticatorService.discard(request.authenticator,
      Ok(okResponse(Messages("logout.success", request.identity.username))))
  }

  def admin: Action[AnyContent] = silhouette.SecuredAction(WithRole("admin")).async { implicit request =>
    userInfoService.findAll().map { list =>
      Ok(views.html.admin(Some(request.identity), list))
    }
  }

  def updateRole(): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    Logger.info(s"updating role")
    val update: Try[Future[Either[String, User]]] = Try {
      val username = request.queryString("username").head
      val roleTo = request.queryString("role").head
      val user: Future[Option[User]] = userInfoService.retrieve(LoginInfo(CredentialsProvider.ID, username))
      require(User.checkRole(roleTo))
      user.flatMap(u => userInfoService.update(u.get.copy(role = roleTo)))
    }
    Future.fromTry(update)
      .flatten
      .map {
        case Right(r) => Ok(okResponse(Messages("add.success", r.username)))
        case Left(r)  => BadRequest(failResponse(Messages("add.failure", r)))
      }
      .recover {
        case e: Exception =>
          println(e.getLocalizedMessage)
          BadRequest(failResponse(Messages("find.failure", e.getMessage)))
      }
  }

  def account = silhouette.SecuredAction { implicit request =>
    Ok(views.html.profile(Some(request.identity)))
  }

  def update: Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    Logger.info("updating information")
    println(request.queryString)
    val query = (key: String) => request.getQueryString(key)
    val user: Option[User] = for {
      firstName <- query("firstName")
      lastName <- query("lastName")
      phoneNumber <- query("phoneNumber")
      email <- query("email")
    } yield request.identity.copy(firstName = firstName, lastName = lastName, phoneNumber = phoneNumber.toInt, email = email)

    Logger.info("user details verified")
    user.map { u =>
      userInfoService
        .update(u)
        .flatMap {
          case Right(_) => Future.successful(Ok("Success"))
          case Left(_)  => Future.successful(Ok("Fail updating"))
        }
    }.getOrElse(Future.successful(Ok("Fail with data")))
  }

  def user(username: String): Action[AnyContent] = silhouette.SecuredAction.async { implicit request =>
    userInfoService
      .retrieve(LoginInfo(CredentialsProvider.ID, username))
      .map(u => Ok(views.html.user(Some(request.identity), u)))
  }

  def authJSRoutes = Action { implicit request =>
    Ok(
      JavaScriptReverseRouter("authJSRoutes")(
        routes.javascript.AuthController.postRegisterJson,
        routes.javascript.AuthController.postLoginJson,
        routes.javascript.AuthController.logout,
        routes.javascript.AuthController.updateRole,
        routes.javascript.AuthController.update
      )
    ).as("text/javascript")
  }
}
