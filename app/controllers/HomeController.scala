package controllers

import javax.inject._

import com.mohiva.play.silhouette.api.Silhouette
import models.Note
import models.auth.User
import play.api.Logger
import play.api.i18n.Messages
import play.api.libs.json.{JsError, JsValue, Json}
import play.api.mvc._
import play.api.routing.JavaScriptReverseRouter
import services.NoteService
import util.{AuthEnv, WithRole}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class HomeController @Inject()(cc: ControllerComponents,
                               noteService: NoteService,
                               silhouette: Silhouette[AuthEnv]) extends AbstractController(cc) with MessageController {

  implicit lazy val ec: ExecutionContext = defaultExecutionContext

  def home() = silhouette.UserAwareAction { implicit request =>
    Ok(views.html.search(request.identity))
  }

  def findNote: Action[AnyContent] = silhouette.UserAwareAction.async { implicit request =>
    noteService.findText(request.queryString("query").head).map(list => Ok(Json.toJson(list)))
      .recover{
        case e: Exception => BadRequest(failResponse(Messages("find.failure", e.getMessage)))
      }
  }

  def getNote(url: String): Action[AnyContent] = silhouette.UserAwareAction.async { implicit request =>
    for {
      link <- noteService.findByURL(url)
    } yield Ok(views.html.note(request.identity, link.get))
  }

  def note: Action[AnyContent] = silhouette.SecuredAction(!WithRole(User.GUEST)) { implicit request =>
    Ok(views.html.post(Some(request.identity)))
  }

  def insertNote(): Action[JsValue] = silhouette
    .SecuredAction(!WithRole(User.GUEST))
    .async(parse.json) { implicit request =>
    request.body.validate[Note].fold(
      error => Future.successful(BadRequest(Json.obj("Status" -> "Fail", "message" -> JsError.toJson(error)))),
      note => {
        noteService.insert(note)
          .map {
            case Right(n) => Ok(okResponse(Messages("add.success", n.username)))
            case Left(s) => BadRequest(failResponse(Messages("add.failure", note.username, s)))
          }
      }
    )
  }

  def about = silhouette.UserAwareAction { implicit request =>
    Logger.info(request.identity.map(_.username).getOrElse("None"))
    Ok(views.html.about(request.identity))
  }
  def homeJSRoutes = Action { implicit request =>
    Ok(
      JavaScriptReverseRouter("homeJSRoutes")(
        routes.javascript.HomeController.findNote,
        routes.javascript.HomeController.insertNote,
        routes.javascript.HomeController.getNote
      )
    ).as("text/javascript")
  }
}